﻿using System;
using System.Collections.Generic;

namespace Noroff_Task_14
{
    class Program
    {
        static void Main(string[] args)
        { 
            FindLagrestPalindrome();
        }

        //Method fo finding the largest palindrome made of the product
        //of two numbers with three 3-digit numbers.
        private static void FindLagrestPalindrome()
        {
            int largestPalindrome = 0;
            for (int i = 999; i > 99; i--)
            {
                for (int j = i; j > 99; j--)
                {
                    largestPalindrome = isPalindrome(i, j, largestPalindrome);
                }
            }
            Console.WriteLine("Largest palindrome: " + largestPalindrome);
        }


        //Checks if the product of two numbers is a palindrome,
        //and compares to current largest palindrome and returns
        //the largest of the two.
        private static int isPalindrome(int i, int j, int largestPalindrome)
        {
            int product = i * j;
            string productString = product.ToString();
            string reversed = Reverse(product.ToString());
            if (productString == reversed)
            {
                if (product > largestPalindrome)
                {
                    largestPalindrome = product;
                }
            }
            return largestPalindrome;
        }

        //Reverse a string to help palindrome-check in oter method
        private static string Reverse(string text)
        {
            char[] cArray = text.ToCharArray();
            string reverse = String.Empty;
            for (int i = cArray.Length - 1; i > -1; i--)
            {
                reverse += cArray[i];
            }
            return reverse;
        }
    }
}
